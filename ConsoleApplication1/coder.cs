﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace Cryptochat
{
    internal static class coder //Класс шифрования
    {
            static byte[] Key = { 0xBF, 0x21, 0xAB, 0x64, 0xDE, 0x64, 0x48, 0xF1, 0x85, 0xF2, 0x82, 0xD9, 0xE9, 0xC9, 0x78, 0x13, 0x0, 0xAA, 0x7F, 0x24, 0xCA, 0xB1, 0xB6, 0xA5, 0xD5, 0x3E, 0x90, 0xB7, 0xC1, 0x18, 0x1B, 0x59 };
            static byte[] IV = { 0xBE, 0x35, 0x47, 0xAB, 0x6C, 0xD0, 0x16, 0xCE, 0x84, 0x31, 0x0, 0x74, 0xA7, 0x1D, 0x7D, 0xA9 };
            static RSACryptoServiceProvider RSAEncoder = new RSACryptoServiceProvider();
            static RSACryptoServiceProvider RSADecoder = new RSACryptoServiceProvider();
            static RSAParameters myPrivateKey = RSADecoder.ExportParameters(true);
            static public RSAParameters myPublicKey = RSADecoder.ExportParameters(false);
            static internal RSAParameters adressatPublicKey;
                           
            static UnicodeEncoding ByteConverter = new UnicodeEncoding();

           // static byte[] dataToEncrypt;
           // static byte[] encryptedData;
           // static byte[] decryptedData;

            static coder()
            {

            }
            static public void initEncoder()
            {
                RSAEncoder.ImportParameters(adressatPublicKey);
            }

            static public void importkey(string key)
            {
                string[] tmp = key.Split(';');

                adressatPublicKey.Exponent = Convert.FromBase64String(tmp[0]);

                adressatPublicKey.Modulus = Convert.FromBase64String(tmp[1]);
                
            }
            static public string exportkey()
            {
                return
                       Convert.ToBase64String(myPublicKey.Exponent) + ";" +

                       Convert.ToBase64String(myPublicKey.Modulus);
                       
            }

            static public string EncryptStr(string a)//зашифровать ключем
            {
                using (Rijndael rnl = Rijndael.Create())
                {
                    // Create a decrytor to perform the stream transform.
                    ICryptoTransform encryptor = rnl.CreateEncryptor(Key, IV);

                    using (MemoryStream msEncrypt = new MemoryStream())
                    {
                        using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                        {
                            byte[] bbb = Encoding.UTF8.GetBytes(a);
                            csEncrypt.Write(bbb, 0, bbb.Length);
                            csEncrypt.FlushFinalBlock();
                            return Convert.ToBase64String(msEncrypt.ToArray());

                        }

                    }
                }
            }
            static public string DecryptStr(string b)//расшифровать ключем
            {
                Rijndael rnl = Rijndael.Create();
                ICryptoTransform decryptor = rnl.CreateDecryptor(Key, IV);
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, decryptor, CryptoStreamMode.Write))
                    {
                        byte[] a = Convert.FromBase64CharArray(b.ToCharArray(), 0, b.Length);
                        csEncrypt.Write(a, 0, a.Length);
                        csEncrypt.FlushFinalBlock();
                        return Encoding.UTF8.GetString(msEncrypt.ToArray());

                    }

                }
            }

            
            /*static public string PrintByteArray(byte[] bytes)
            {
                var sb = new StringBuilder("{ ");
                foreach (var b in bytes)
                {
                    sb.Append("0x" + b.ToString("X") + ", ");
                }
                sb.Append("}");
                return sb.ToString();
            }*/
            static public string RSAEncrypt(string DataToEncrypt)
            {
                try
                {
                    byte[] encryptedData;
                    //Create a new instance of RSACryptoServiceProvider.
                    

                        //Import the RSA Key information. This only needs
                        //toinclude the public key information.
                        RSAEncoder.ImportParameters(adressatPublicKey);

                        //Encrypt the passed byte array and specify OAEP padding.  
                        //OAEP padding is only available on Microsoft Windows XP or
                        //later.  
                        encryptedData = RSAEncoder.Encrypt(Encoding.UTF8.GetBytes(DataToEncrypt), false);

                        return Convert.ToBase64String(encryptedData);
                }
                //Catch and display a CryptographicException  
                //to the console.
                catch (CryptographicException e)
                {
                    Console.WriteLine(e.Message);

                    return null;
                }

            }

            static public string RSADecrypt(string DataToDecrypt)
            {
                try
                {
                    byte[] decryptedData;
                    byte[] message = Convert.FromBase64CharArray(DataToDecrypt.ToCharArray(), 0, DataToDecrypt.Length);
                    //Create a new instance of RSACryptoServiceProvider.
                    
                        //Import the RSA Key information. This needs
                        //to include the private key information.
                        RSADecoder.ImportParameters(myPrivateKey);

                        //Decrypt the passed byte array and specify OAEP padding.  
                        //OAEP padding is only available on Microsoft Windows XP or
                        //later.  
                        decryptedData = RSADecoder.Decrypt(message, false);

                        return Encoding.UTF8.GetString(decryptedData);
                }
                //Catch and display a CryptographicException  
                //to the console.
                catch (CryptographicException e)
                {
                    Console.WriteLine(e.ToString());

                    return null;
                }

            }
    }
}
