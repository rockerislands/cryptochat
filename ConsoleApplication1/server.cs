﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;

namespace Cryptochat
{
    partial class Program
    {
        private static void parseRecived(IPEndPoint receivedIpEndPoint, Byte[] receivedBytes)
        {
            string receivedText = Encoding.UTF8.GetString(receivedBytes);
            if (debug) print(receivedText,ConsoleColor.DarkRed);
            char[] sep = { '#' };
            string[] text = receivedText.Split(sep, 3);
            if (text[0]=="private")
            {
                if (!private_mode)
                {
                    print("Private chat with :" + receivedIpEndPoint, ConsoleColor.DarkRed);
                    //string gopriv = Console.ReadLine();
                    if (true)//gopriv == "y") 
                    { 
                        private_mode = true;
                        private_adress = Dns.GetHostEntry(receivedIpEndPoint.Address).HostName;
                        coder.importkey(text[1]);
                        string msg ="readytoprivate#"+coder.exportkey();
                        Thread sendmsg = new Thread(SendMessageTo);
                        sendmsg.Start(new MessageObj(Encoding.UTF8.GetBytes(msg), private_adress));
                    }
                    //if (gopriv == "n")
                    //{
                    //    string msg = "cantprivatenow#";
                    //    msg = coder.EncryptStr(msg);
                    //    SendMessageTo(Encoding.UTF8.GetBytes(msg), receivedIpEndPoint.ToString());
                    //}
                }
                else
                {
                    string msg = "cantprivatenow#";
                    msg = coder.EncryptStr(msg);
                    Thread sendmsg = new Thread(SendMessageTo);
                    sendmsg.Start(new MessageObj(Encoding.UTF8.GetBytes(msg), private_adress));
                }
                return;
            }
            if (text[0] == "cantprivatenow")
            {
                private_mode = false;
                print("Adrressat busy now",ConsoleColor.DarkRed);
                return;
            }
            if (text[0] == "readytoprivate")
            {
                private_mode = true;
                print("Private mode with: "+private_adress+" started. To terminate type -private",ConsoleColor.Red);
                coder.importkey(text[1]);
            }
            if (private_mode&&text[0] == "Rmode")
            {
                string msg = coder.RSADecrypt(text[1]);
                if (debug) print(msg,ConsoleColor.DarkRed);
                string[] tmp = msg.Split('#');
                //string txt = receivedIpEndPoint + " | " + tmp[1] + ": " + tmp[2];
                string decrTxt;
                if (debug) 
                {
                    print(text[1], UInt16.Parse(tmp[0]));
                    decrTxt = receivedIpEndPoint + " | " + tmp[1] + ": " + tmp[2];
                }
                else
                {
                    decrTxt = receivedIpEndPoint + " | " + tmp[1] + ": " + tmp[2].Split('#')[0];
                }
                print(decrTxt, UInt16.Parse(tmp[0]));
            }
            if (text.Length == 3)
            {
                string txt = receivedIpEndPoint + " | " + text[1] + ": " + text[2];
                if(debug) print(txt, UInt16.Parse(text[0]));
                string decrTxt = receivedIpEndPoint + " | " + text[1] + ": " + coder.DecryptStr(text[2]);
                print(decrTxt, UInt16.Parse(text[0]));
            }
        }

        private static void print(string text, ushort Color)
        {
            Console.ForegroundColor = (ConsoleColor)Color;
            Console.WriteLine(text);
            Console.ForegroundColor = (ConsoleColor)color;
        }
        public static void print(string text, ConsoleColor Color)
        {
            Console.ForegroundColor = Color;
            Console.WriteLine(text);
            Console.ForegroundColor = (ConsoleColor)color;
        }
    }
}
