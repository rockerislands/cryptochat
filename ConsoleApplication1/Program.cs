﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.DirectoryServices;
using System.Collections.Generic;
using ListNetworkComputers;
using System.Threading;
using System.Diagnostics;
using System.Collections;

namespace Cryptochat
{
    partial class Program
    {
        static void Main(string[] args)
        {
            print("Enter your name: ", ConsoleColor.Yellow);
            string name = Console.ReadLine();
            if (!Receivers.CreateReceivers(serverPort))
            {
                Console.WriteLine("Невозможно создать сервер");
                Console.ReadKey();
                return;
            }
            // Start async receiving
            Receivers.BeginReceive(DataReceived);

            while (true)
            {
                Console.ForegroundColor = (ConsoleColor)color;
                string str = Console.ReadLine();

                try
                {
                    //parse commands
                    if (str.Length > 0 && str[0].Equals('-'))
                    {
                        if (str.Equals("-exit")) break;
                        if (str.StartsWith("-setcolor"))
                        {
                            char[] sep = { ' ' };
                            string[] pstr = str.Split(sep, 2);
                            if (pstr.Length >= 2)
                            {
                                short newcolor = Int16.Parse(pstr[1]);
                                if (newcolor > 0 && newcolor < 16)
                                { color = (ushort)newcolor; }
                                else
                                {
                                    print("Usage: -setcolor [1-15]", ConsoleColor.Red);
                                }
                            }

                        }
                        if (str.StartsWith("-online"))
                        {
                            NetworkBrowser nb = new NetworkBrowser();
                            ArrayList pcs = nb.getNetworkComputers();

                            foreach (string pc in pcs)
                            {
                                print(pc,ConsoleColor.DarkYellow);
                            }
                        };
                        if (str.StartsWith("-private")) 
                        {
                            private_mode = !private_mode;
                            if (private_mode)
                            {
                                string[] tmp = str.Split();
                                private_adress = tmp[1];
                                string msg = "private#" + coder.exportkey();
                                //msg = coder.EncryptStr(msg);
                                Thread sendmsg = new Thread(SendMessageTo);
                                sendmsg.Start(new MessageObj(Encoding.UTF8.GetBytes(msg), private_adress));
                            }
                        }
                                              
                        if (str.StartsWith("-debug")) { debug = !debug; print("#DEBUG MODE:"+debug+" #",ConsoleColor.Red); }

                        if (str.StartsWith("-help")) print("-help\n-setcolor\n-online*\n-private\n-debug", ConsoleColor.Red);

                    }

                    else //send message
                    {
                        if (!private_mode)
                        {
                            Thread send = new Thread(FindServers);
                            str = coder.EncryptStr(str + "#" + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString());
                            send.Start(color + "#" + name + "#" + str );
                        }
                        else
                        {
                            str = coder.RSAEncrypt(color + "#" + name + "#" + str);
                            str = "Rmode#" + str;
                            Thread sendmsg = new Thread(SendMessageTo);
                            sendmsg.Start(new MessageObj(Encoding.UTF8.GetBytes(str), private_adress));
                        }
                    }
                }
                catch (Exception e) { print(e.Message, ConsoleColor.Red); }
            }
        }

 
    }
}