﻿using ListNetworkComputers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Diagnostics;

namespace Cryptochat
{
    partial class Program
    {
        static void FindServers(object message)
        {
            NetworkBrowser nb = new NetworkBrowser();
            byte[] data = Encoding.UTF8.GetBytes((string)message);

            ArrayList pcs = nb.getNetworkComputers();

            foreach (string pc in pcs)
            {
                Thread sendmsg = new Thread(SendMessageTo);
                sendmsg.Start(new MessageObj(data,pc));
            }
        }

        private static void SendMessageTo(object msgotobj)//(byte[] data, string pc)
        {
            MessageObj msgto = (MessageObj)msgotobj;
            try
            {
                IPEndPoint addr = msgto.addr == null ? new IPEndPoint(GetIPv4(Dns.GetHostAddresses(msgto.pc)), serverPort) : msgto.addr;
                if (debug) print("sending to: " + msgto.pc + ":" + addr.Address + ":" + addr.Port + ":\n " + Encoding.UTF8.GetString(msgto.data), ConsoleColor.DarkRed);
                if (debug || (addr.Address.ToString() != MyIP.ToString()))
                {
                    client.Send(msgto.data, msgto.data.Length, addr);
                }
            }
            catch (Exception e) { print(e.Message, ConsoleColor.Red); }

        }
        static IPAddress GetIPv4(IPAddress[] arr)
        {
            foreach (IPAddress a in arr)
                if (a.AddressFamily == AddressFamily.InterNetwork) return a;
            return null;
        }

        private static void DataReceived(IAsyncResult ar)
        {
            UdpClient receiver = (UdpClient)ar.AsyncState;
            IPEndPoint receivedIpEndPoint = new IPEndPoint(IPAddress.Any, 0);
            Byte[] receivedBytes = receiver.EndReceive(ar, ref receivedIpEndPoint);

            parseRecived(receivedIpEndPoint, receivedBytes);
            receiver.BeginReceive(DataReceived, ar.AsyncState);
        }

    }
}
