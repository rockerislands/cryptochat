﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Cryptochat
{
    class MessageObj
    {
        public string pc;
        public IPEndPoint addr;
        public byte[] data;
        public MessageObj(byte[] d, string pcname)
        {
            addr = null;
            pc = pcname;
            data = d;
        }
        public MessageObj(byte[] d, IPEndPoint endpoint)
        {
            pc = null;
            addr = endpoint;
            data = d;
        }
    }
    static class Receivers
    {
        static List<UdpClient> recvs;
        static public void BeginReceive(AsyncCallback cb)
        {
            foreach (UdpClient server in recvs)
            {
                Program.print("Starting upd receiving on : " + server.Client.LocalEndPoint.ToString(), ConsoleColor.Yellow);
                server.BeginReceive(cb, server);
            }
            Program.print("\n-------------------------------\n", ConsoleColor.Yellow);
        }
        static public bool CreateReceivers(ushort port)
        {
            recvs = new List<UdpClient>();
            IPAddress[] local = Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList;
            foreach (IPAddress a in local)
            {
                try
                {
                    if (a.AddressFamily == AddressFamily.InterNetwork) recvs.Add(new UdpClient(new IPEndPoint(a, port)));
                }
                catch (SocketException e)
                {
                    if (e.ErrorCode == 10048)
                        Console.WriteLine("Порт " + port + " на адресе " + a + " уже занят");
                    else Console.WriteLine(e.Message);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            if (recvs.Count == 0) return false;
            return true;
        }
    }
    partial class Program
    {
        //coder encr = new coder();

        static bool debug = false;
        static bool private_mode = false;

        static string private_adress = "";

        static IPAddress MyIP = GetIPv4(Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList);
        static ushort serverPort = 49000;

        static UdpClient client = new UdpClient();
        static ushort color = (byte)ConsoleColor.White;

#if DEBUG
        Stopwatch sw = new Stopwatch();
#endif
    }
}
